
SIMULATOR:="./pokemon-showdown/"
SPRITES:="./sprites/"
ASCIISPRITES:="./asciisprites/"

launch: $(SIMULATOR) $(ASCIISPRITES) launcher.sh output.awk
	@clear
	#@cat README.md
	@echo "help will be displayed here..."
	@read -p "press enter to continue..." var
	chmod +x launcher.sh output.awk && ./launcher.sh

$(SIMULATOR):
	[ -d $@ ] || ( git clone https://github.com/smogon/pokemon-showdown.git && cd ./pokemon-showdown/ && ./build )

$(SPRITES):
	[ -d $@ ] || git clone https://github.com/PokeAPI/sprites.git

pokemon2id.json:
	curl https://pokeapi.co/api/v2/pokemon?limit=10000 | jq '.results|map({"key":.name, "value":.url})|from_entries' | awk '/http/{print gensub(/"http.*pokemon\/([0-9]+)\/"/, "\\1", 1)}!/http/' > $@

$(ASCIISPRITES): $(SPRITES) pokemon2id.json
	[ -d $@ ] || ( mkdir -p $@ && chmod +x build_sprites.sh && ./build_sprites.sh )
