#!/bin/bash

sources=$(jq '.|keys[]' <pokemon2id.json)
total=$(echo $sources | wc -w)
cpt=0
bar="#####################################################"

for pokemon in $sources
  do
  id=$(jq .$pokemon <pokemon2id.json)
  source="./sprites/sprites/pokemon/other/official-artwork/$id.png" 
  target=$(echo "./asciisprites/$pokemon.ascii" | tr -d \")
  [ -f $source ] &&
  convert $source -background black -flatten jpeg:- |
  jp2a --color-depth=8 --height=17 - >$target
  ((cpt++))
  printf "[\e[92m%-${#bar}s\e[0m] %s               \r" "${bar:0:$((${#bar}*$cpt/$total))}" $pokemon
  done
