#!/bin/gawk -f

# pokemon showdown battle simulator parser

BEGIN {
  # banner
  system("figlet -f smslant -w $COLUMNS Asciimon Battle Simulator")
  p1=p2=""
  RS=ORS="\n\n"
  FS=OFS="\n"
}

/^update/ {
  # main loop
  print $0
  print_image_local(p1)
  print_image_local(p2)
  p1=p2=""
}

/request/ {
  split($0, split_result, "|")
  if (p1=="") {
    p1 = split_result[3]
    next
  }
  if (p2=="") {
    p2 = split_result[3]
    next
  }
}

/^>$/

function json_prompt(json) {
  tmp =  "jq -C --tab '{active:.side.pokemon[]|select(.active).ident, moves:[.active[].moves[].move], party:.side.pokemon|map({key:.details,value:.condition})|from_entries}'"
  print json |& tmp
  close(tmp, "to")
  tmp |& getline formated
  close(tmp)
  return formated
}

function get_active(json) {
  #match(prompt, /"active": "p.: ([[:alpha:]]+)",\n/, pats)
  #return pats[1]
  tmp = "jq -r '.side.pokemon[]|select(.active).details' | cut -d, -f1"
  print json |& tmp
  close(tmp, "to")
  tmp |& getline pokemon
  close(tmp)
  match(pokemon, /([a-zA-Z]+)/, pat)
  sani=pat[1]
  return sani
}

# deprecated -> get image from pokeapi and convert it on the fly which is slow and unefficient
function print_image(pokemon) {
  system("curl -s https://pokeapi.co/api/v2/pokemon/"tolower(pokemon)" | jq [.sprites.other[]][2].front_default | xargs curl -s | convert png:- -background black -flatten jpeg:- | jp2a - --height=30 --color")
}

function print_image_local(json) {
  print "======================================"
  parsed=json_prompt(json)
  pokemon=get_active(json)
  system("echo '"parsed"' | pr -tms'|' -o3 asciisprites/"tolower(pokemon)".ascii - ")
}
